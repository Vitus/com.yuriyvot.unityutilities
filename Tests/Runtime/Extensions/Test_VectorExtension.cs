using NUnit.Framework;
using Shouldly;
using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_VectorExtension
    {
        private Vector2 _vector2;
        private Vector3 _vector3;

        [SetUp]
        public void SetUp ()
        {
            _vector2 = new Vector2 (4, 5);
            _vector3 = new Vector3 (1, 2, 3);
        }

        [Test]
        public void ToXZVector3 ()
        {
            _vector2.ToXZVector3 ().ShouldBe (new Vector3 (4, 0, 5));
        }

        [Test]
        public void ToVector2ByXZ ()
        {
            _vector3.ToVector2ByXZ ().ShouldBe (new Vector2 (1, 3));
        }

        [Test]
        public void Vector2_WithX ()
        {
            _vector2.WithX (10).ShouldBe (new Vector2 (10, 5));
        }

        [Test]
        public void Vector2_WithY ()
        {
            _vector2.WithY (10).ShouldBe (new Vector2 (4, 10));
        }

        [Test]
        public void Vector3_WithX ()
        {
            _vector3.WithX (10).ShouldBe (new Vector3 (10, 2, 3));
        }

        [Test]
        public void Vector3_WithY ()
        {
            _vector3.WithY (10).ShouldBe (new Vector3 (1, 10, 3));
        }

        [Test]
        public void Vector3_WithZ ()
        {
            _vector3.WithZ (10).ShouldBe (new Vector3 (1, 2, 10));
        }

        [Test]
        public void Vector2_ToStringWithMoreThanZeroDigits ()
        {
            _vector2.ToStringWithDigits (3).ShouldBe ("(4.000, 5.000)");
        }

        [Test]
        public void Vector2_ToStringWithZeroDigits ()
        {
            _vector2.ToStringWithDigits (0).ShouldBe ("(4, 5)");
        }

        [Test]
        public void Vector3_ToStringWithMoreThanZeroDigits ()
        {
            _vector3.ToStringWithDigits (3).ShouldBe ("(1.000, 2.000, 3.000)");
        }

        [Test]
        public void Vector3_ToStringWithZeroDigits ()
        {
            _vector3.ToStringWithDigits (0).ShouldBe ("(1, 2, 3)");
        }

        [Test]
        public void Vector2_RotateClockwise ()
        {
            var rotatedVector = Vector2.right.RotateClockwise (-45f);
            rotatedVector.x.ShouldBe (0.707f, 0.01f);
            rotatedVector.y.ShouldBe (0.707f, 0.01f);
        }
    }
}
