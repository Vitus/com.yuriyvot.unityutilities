﻿using System.Collections.Generic;
using NUnit.Framework;
using Shouldly;
using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_GameObjectExtension
    {
        private GameObject _gameObject;

        [SetUp]
        public void SetUp ()
        {
            _gameObject = new GameObject ("TestObject");
        }

        [TearDown]
        public void TearDown ()
        {
            Object.DestroyImmediate (_gameObject);
        }

        [Test]
        public void GetOrAddComponent_ShouldAddComponentToGameObject_WhenComponentNotExists ()
        {
            var addedComponent = _gameObject.GetOrAddComponent<TestComponent> ();

            addedComponent.ShouldNotBeNull ();
            addedComponent.ShouldBeOfType<TestComponent> ();
            addedComponent.gameObject.ShouldBe (_gameObject);
        }

        [Test]
        public void GetOrAddComponent_ShowGetComponentOnGameObject_WhenComponentExists ()
        {
            var existedComponent = _gameObject.AddComponent<TestComponent> ();
            var searchedComponent = _gameObject.GetOrAddComponent<TestComponent> ();

            searchedComponent.ShouldBe (existedComponent);
        }

        public class TestComponent : MonoBehaviour { }
    }
}
