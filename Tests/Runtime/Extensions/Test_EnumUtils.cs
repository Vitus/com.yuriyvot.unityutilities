using NSubstitute;
using NUnit.Framework;
using Shouldly;
using YuriyVot.UnityUtilities.Wrappers;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_EnumUtils
    {
        [TestCase (TestEnum.Element1, 0)]
        [TestCase (TestEnum.Element2, 1)]
        [TestCase (TestEnum.Element3, 2)]
        public void Random_ShouldReturnRandomEnum (TestEnum enumValue, int enumValueIndex)
        {
            var randomStub = Substitute.For<IRandom> ();
            randomStub.Range (Arg.Any<int> (), Arg.Any<int> ()).Returns (enumValueIndex);

            var randomElement = EnumUtils.Random<TestEnum> (randomStub);

            randomElement.ShouldBe (enumValue);
        }

        public enum TestEnum
        {
            Element1,
            Element2,
            Element3
        }
    }
}
