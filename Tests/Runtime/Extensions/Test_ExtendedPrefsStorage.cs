﻿using NSubstitute;
using NUnit.Framework;
using Shouldly;
using UnityEngine;
using YuriyVot.UnityUtilities.ExtendedPlayerPrefs;
using YuriyVot.UnityUtilities.Wrappers;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_ExtendedPrefsStorage
    {
        private IPrefsStorage _storage;
        private ExtendedPrefsStorage _extendedStorage;

        [SetUp]
        public void SetUp ()
        {
            _storage = Substitute.For<IPrefsStorage> ();
            _extendedStorage = new ExtendedPrefsStorage (_storage);
        }

        [Test]
        public void HasKey_ShouldFindVectorValues ()
        {
            _extendedStorage.HasKey ("vector").ShouldBe (false);
            _storage.HasKey ("vector[.x]").Returns (true);
            _extendedStorage.HasKey ("vector").ShouldBe (true);
        }

        [Test]
        public void DeleteKey_ShouldDeleteAllVectorComponent ()
        {
            _extendedStorage.DeleteKey ("vector");
            _storage.Received ().DeleteKey ("vector");
            _storage.Received ().DeleteKey ("vector[.x]");
            _storage.Received ().DeleteKey ("vector[.y]");
            _storage.Received ().DeleteKey ("vector[.z]");
        }

        [Test]
        public void SetBool_ShouldStoreValueAsInt ()
        {
            _storage.DidNotReceiveWithAnyArgs ().SetInt (default, default);
            _extendedStorage.SetBool ("bool", true);
            _storage.Received ().SetInt ("bool", 1);
        }

        [Test]
        public void GetBool_ShouldGetValueAsInt ()
        {
            _storage.GetInt ("bool", 1).Returns (0);
            _storage.DidNotReceiveWithAnyArgs ().GetInt (default, default);
            var result = _extendedStorage.GetBool ("bool", true);
            _storage.Received ().GetInt ("bool", 1);
            result.ShouldBe (false);
        }

        [Test]
        public void SetVector_ShouldStoreValueAsSeveralFloats ()
        {
            _storage.DidNotReceiveWithAnyArgs ().SetFloat (default, default);
            _extendedStorage.SetVector3 ("vector", new Vector3 (1, 2, 3));
            _storage.Received ().SetFloat ("vector[.x]", 1);
            _storage.Received ().SetFloat ("vector[.y]", 2);
            _storage.Received ().SetFloat ("vector[.z]", 3);
        }

        [Test]
        public void GetVector3_ShouldGetValueFromSeveralFloats ()
        {
            _storage.GetFloat ("vector[.x]", Arg.Any<float> ()).Returns (1);
            _storage.GetFloat ("vector[.y]", Arg.Any<float> ()).Returns (2);
            _storage.GetFloat ("vector[.z]", Arg.Any<float> ()).Returns (3);
            _storage.DidNotReceiveWithAnyArgs ().GetFloat (default, default);
            var result = _extendedStorage.GetVector3 ("vector", new Vector3 (4, 5, 6));
            _storage.Received ().GetFloat ("vector[.x]", 4);
            _storage.Received ().GetFloat ("vector[.y]", 5);
            _storage.Received ().GetFloat ("vector[.z]", 6);
            result.ShouldBe (new Vector3 (1, 2, 3));
        }

        [Test]
        public void DeleteAll_ShouldMirrorStorageMethod ()
        {
            _storage.DidNotReceive ().DeleteAll ();
            _extendedStorage.DeleteAll ();
            _storage.Received ().DeleteAll ();
        }

        [Test]
        public void GetFloat_ShouldMirrorStorageMethod ()
        {
            _storage.DidNotReceiveWithAnyArgs ().GetFloat (default, default);
            _extendedStorage.GetFloat ("float", 1f);
            _storage.Received ().GetFloat ("float", 1f);
        }

        [Test]
        public void SetFloat_ShouldMirrorStorageMethod ()
        {
            _storage.DidNotReceiveWithAnyArgs ().SetFloat (default, default);
            _extendedStorage.SetFloat ("float", 1f);
            _storage.Received ().SetFloat ("float", 1f);
        }

        [Test]
        public void GetString_ShouldMirrorStorageMethod ()
        {
            _storage.DidNotReceiveWithAnyArgs ().GetString (default, default);
            _extendedStorage.GetString ("string", "a");
            _storage.Received ().GetString ("string", "a");
        }

        [Test]
        public void SetString_ShouldMirrorStorageMethod ()
        {
            _storage.DidNotReceiveWithAnyArgs ().SetString (default, default);
            _extendedStorage.SetString ("string", "a");
            _storage.Received ().SetString ("string", "a");
        }

        [Test]
        public void GetInt_ShouldMirrorStorageMethod ()
        {
            _storage.DidNotReceiveWithAnyArgs ().GetInt (default, default);
            _extendedStorage.GetInt ("string", 1);
            _storage.Received ().GetInt ("string", 1);
        }

        [Test]
        public void SetInt_ShouldMirrorStorageMethod ()
        {
            _storage.DidNotReceiveWithAnyArgs ().SetInt (default, default);
            _extendedStorage.SetInt ("string", 1);
            _storage.Received ().SetInt ("string", 1);
        }
    }
}
