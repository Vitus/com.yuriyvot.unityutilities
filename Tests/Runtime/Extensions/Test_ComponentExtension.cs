﻿using System.Collections.Generic;
using NUnit.Framework;
using Shouldly;
using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_ComponentExtension
    {
        private GameObject _gameObject;
        private TestComponent1 _initialComponent;

        [SetUp]
        public void SetUp ()
        {
            _gameObject = new GameObject ("TestObject");
            _initialComponent = _gameObject.AddComponent<TestComponent1> ();
        }

        [TearDown]
        public void TearDown ()
        {
            Object.DestroyImmediate (_gameObject);
        }

        [Test]
        public void GetOrAddComponent_ShouldAddComponentToGameObject_WhenComponentNotExists ()
        {
            var addedComponent = _initialComponent.GetOrAddComponent<TestComponent2> ();

            addedComponent.ShouldNotBeNull ();
            addedComponent.ShouldBeOfType<TestComponent2> ();
            addedComponent.gameObject.ShouldBe (_gameObject);
        }

        [Test]
        public void GetOrAddComponent_ShowGetComponentOnGameObject_WhenComponentExists ()
        {
            var existedComponent = _gameObject.AddComponent<TestComponent2> ();
            var searchedComponent = _initialComponent.GetOrAddComponent<TestComponent2> ();

            searchedComponent.ShouldBe (existedComponent);
        }

        [Test]
        public void GetComponentNonAlloc_ShouldReturnExistedComponent ()
        {
            var buffer = new List<TestComponent1> ();
            var component = _initialComponent.GetComponentNonAlloc (buffer);

            component.ShouldBe (_initialComponent);
        }

        [Test]
        public void GetComponentNonAlloc_ShouldReturnNull_WhenComponentNotFound ()
        {
            var otherGameObject = new GameObject("Other TestObject");
            var otherComponent = otherGameObject.AddComponent<TestComponent2> ();
            var buffer = new List<TestComponent2> {otherComponent};

            var component = _initialComponent.GetComponentNonAlloc (buffer);

            component.ShouldBeNull ();

            Object.DestroyImmediate (otherGameObject);
        }

        public class TestComponent1 : MonoBehaviour { }

        public class TestComponent2 : MonoBehaviour { }
    }
}
