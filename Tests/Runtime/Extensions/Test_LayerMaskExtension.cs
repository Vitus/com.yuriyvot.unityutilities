using NUnit.Framework;
using Shouldly;
using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_LayerMaskExtension
    {
        [TestCase(0, true)]
        [TestCase(1, false)]
        public void Contains_ShouldCheckLayerAddedToMask (int layer, bool contains)
        {
            var layerMask = new LayerMask ();
            layerMask.value = 0b0101;

            layerMask.Contains (layer).ShouldBe (contains);
        }
    }
}
