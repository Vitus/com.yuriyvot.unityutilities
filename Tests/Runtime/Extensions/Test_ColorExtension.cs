﻿using NUnit.Framework;
using Shouldly;
using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_ColorExtension
    {
        private Color _initialColor;

        [SetUp]
        public void SetUp ()
        {
            _initialColor = new Color (0.58f, 0.53f, 0.78f, 0.75f);
        }

        [Test]
        public void WithRed_ChangesRedComponentOfColor()
        {
            var modifiedColor = _initialColor.WithRed (0.4f);
            modifiedColor.ShouldBe (new Color (0.4f, 0.53f, 0.78f, 0.75f));
        }

        [Test]
        public void WithGreen_ChangesGreenComponentOfColor ()
        {
            var modifiedColor = _initialColor.WithGreen (0.4f);
            modifiedColor.ShouldBe (new Color (0.58f, 0.4f, 0.78f, 0.75f));
        }

        [Test]
        public void WithBlue_ChangesBlueComponentOfColor ()
        {
            var modifiedColor = _initialColor.WithBlue (0.4f);
            modifiedColor.ShouldBe (new Color (0.58f, 0.53f, 0.4f, 0.75f));
        }

        [Test]
        public void WithAlpha_ChangesAlphaComponentOfColor ()
        {
            var modifiedColor = _initialColor.WithAlpha (0.4f);
            modifiedColor.ShouldBe (new Color (0.58f, 0.53f, 0.78f, 0.4f));
        }
    }
}
