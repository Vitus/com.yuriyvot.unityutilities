using System;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using YuriyVot.UnityUtilities.Wrappers;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_ListExtension
    {
        private IRandom _random;
        private List<int> _list;

        [SetUp]
        public void SetUp ()
        {
            _random = Substitute.For<IRandom> ();
            _list = new List<int> ();
        }

        [Test]
        public void Random_ShouldThrow_WhenListEmpty ()
        {
            var list = new List<int> ();
            Should.Throw<IndexOutOfRangeException> (() => list.Random (_random));
        }

        [TestCase(0, 1)]
        [TestCase(2, 7)]
        [TestCase(4, 5)]
        public void Random_ShouldReturnRandomElement_WhenListIsNotEmpty (int randomValue, int expectedValue)
        {
            _random.Range (Arg.Any<int> (), Arg.Any<int> ()).Returns (randomValue);
            _list.AddRange (new []{1, 2, 7, 4, 5});

            _list.Random (_random).ShouldBe (expectedValue);
        }

        [Test]
        public void RandomOrDefault_ShouldReturnDefaultValue_WhenListEmpty ()
        {
            _list.RandomOrDefault (_random).ShouldBe (0);
        }

        [TestCase (0, 1)]
        [TestCase (2, 7)]
        [TestCase (4, 5)]
        public void RandomOrDefault_ShouldReturnRandomElement_WhenListIsNotEmpty (int randomValue, int expectedValue)
        {
            _random.Range (Arg.Any<int> (), Arg.Any<int> ()).Returns (randomValue);
            _list.AddRange (new[] {1, 2, 7, 4, 5});

            _list.RandomOrDefault (_random).ShouldBe (expectedValue);
        }

        [Test]
        public void PopLast_ShouldThrow_WhenListIsEmpty ()
        {
            Should.Throw<InvalidOperationException> (() => _list.PopLast ());
        }

        [Test]
        public void PopLast_ShouldReturnLastElementAndRemoveItFromEndOfTheList ()
        {
            _list.AddRange (new[]{1, 2, 3});

            var last1 = _list.PopLast ();
            last1.ShouldBe (3);
            _list.ShouldBe (new []{1, 2});

            var last2 = _list.PopLast ();
            last2.ShouldBe (2);
            _list.ShouldBe (new []{1});

            var last3 = _list.PopLast ();
            last3.ShouldBe (1);
            _list.ShouldBeEmpty ();
        }
    }
}
