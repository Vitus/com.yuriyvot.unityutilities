using NUnit.Framework;
using Shouldly;
using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions.Tests
{
    public class Test_TransformExtension
    {
        private Transform _child;
        private Transform _parent;

        [SetUp]
        public void SetUp ()
        {
            _child = new GameObject ("Child").transform;
            _parent = new GameObject ("Parent").transform;

            _child.SetParent (_parent);

            _parent.position = Vector3.one;

            _child.localPosition = Vector3.one;
            _child.localRotation = Quaternion.Euler (4, 5, 6);
        }

        [TearDown]
        public void TearDown ()
        {
            Object.DestroyImmediate (_child.gameObject);
            Object.DestroyImmediate (_parent.gameObject);
        }

        [Test]
        public void ResetWorldScale ()
        {
            _child.localRotation = Quaternion.identity;
            _parent.localScale = new Vector3 (1, 2, 4);

            _child.ResetWorldScale ();
            Vector3IsEqual (_child.localScale, new Vector3 (1f, 0.5f, 0.25f));
        }

        [Test]
        public void SetUniformWorldScale ()
        {
            _child.localRotation = Quaternion.identity;
            _parent.localScale = new Vector3 (1, 2, 4);

            _child.SetUniformWorldScale (2);
            Vector3IsEqual (_child.localScale, new Vector3 (2f, 1f, 0.5f));
        }

        [Test]
        public void SetWorldScale ()
        {
            _child.localRotation = Quaternion.identity;
            _parent.localScale = new Vector3 (1, 2, 4);

            _child.SetWorldScale (4, 2, 1);
            Vector3IsEqual (_child.localScale, new Vector3 (4f, 1f, 0.25f));
        }

        [Test]
        public void ResetLocation ()
        {
            _parent.rotation = Quaternion.Euler (1, 2, 3);

            _child.ResetLocation ();
            Vector3IsEqual (_child.position, Vector3.zero);
            QuaternionIsEqual (_child.rotation, Quaternion.identity);
        }

        [Test]
        public void ResetLocalLocation ()
        {
            _child.ResetLocalLocation ();
            _child.localPosition.ShouldBe (Vector3.zero);
            _child.localRotation.ShouldBe (Quaternion.identity);
        }

        [Test]
        public void ResetLocalLocationAndScale ()
        {
            _child.localScale = new Vector3 (3, 4, 5);
            _child.ResetLocalLocationAndScale ();
            _child.localPosition.ShouldBe (Vector3.zero);
            _child.localRotation.ShouldBe (Quaternion.identity);
            _child.localScale.ShouldBe (Vector3.one);
        }

        [Test]
        public void CopyLocation ()
        {
            var otherTransform = new GameObject ("other").transform;

            var targetPosition = new Vector3 (7, 8, 9);
            var targetRotation = Quaternion.Euler (9, 8, 7);

            otherTransform.position = targetPosition;
            otherTransform.rotation = targetRotation;

            _child.CopyLocation (otherTransform);

            Vector3IsEqual (_child.position, targetPosition);
            QuaternionIsEqual(_child.rotation, targetRotation);

            Object.DestroyImmediate (otherTransform.gameObject);
        }

        [Test]
        public void CopyLocalLocation ()
        {
            var otherTransform = new GameObject ("other").transform;
            otherTransform.SetParent (_parent);

            var targetPosition = new Vector3 (7, 8, 9);
            var targetRotation = Quaternion.Euler (9, 8, 7);

            otherTransform.localPosition = targetPosition;
            otherTransform.localRotation = targetRotation;

            _child.CopyLocalLocation (otherTransform);

            Vector3IsEqual (_child.localPosition, targetPosition);
            QuaternionIsEqual(_child.localRotation, targetRotation);

            Object.DestroyImmediate (otherTransform.gameObject);
        }

        [Test]
        public void CopyLocalLocationAndScale ()
        {
            var otherTransform = new GameObject ("other").transform;
            otherTransform.SetParent (_parent);

            var targetPosition = new Vector3 (7, 8, 9);
            var targetRotation = Quaternion.Euler (9, 8, 7);
            var targetScale = new Vector3 (10, 11, 12);

            otherTransform.localPosition = targetPosition;
            otherTransform.localRotation = targetRotation;
            otherTransform.localScale = targetScale;

            _child.CopyLocalLocationAndScale (otherTransform);

            Vector3IsEqual (_child.localPosition, targetPosition);
            QuaternionIsEqual(_child.localRotation, targetRotation);
            Vector3IsEqual (_child.localScale, targetScale);

            Object.DestroyImmediate (otherTransform.gameObject);
        }

        [Test]
        public void SetXLocalScale ()
        {
            _child.SetXLocalScale (-1);
            Vector3IsEqual (_child.localScale, new Vector3 (-1, 1, 1));
        }

        [Test]
        public void SetYLocalScale ()
        {
            _child.SetYLocalScale (-1);
            Vector3IsEqual (_child.localScale, new Vector3 (1, -1, 1));
        }

        [Test]
        public void SetZLocalScale ()
        {
            _child.SetZLocalScale (-1);
            Vector3IsEqual (_child.localScale, new Vector3 (1, 1, -1));
        }

        [Test]
        public void SetUniformLocalScale ()
        {
            _child.SetUniformLocalScale (-1);
            Vector3IsEqual (_child.localScale, new Vector3 (-1, -1, -1));
        }

        [Test]
        public void SetXPosition ()
        {
            _parent.rotation = Quaternion.identity;
            _child.SetXPosition(-1);
            Vector3IsEqual (_child.position, new Vector3 (-1, 2, 2));
        }

        [Test]
        public void SetYPosition ()
        {
            _parent.rotation = Quaternion.identity;
            _child.SetYPosition(-1);
            Vector3IsEqual (_child.position, new Vector3 (2, -1, 2));
        }

        [Test]
        public void SetZPosition ()
        {
            _parent.rotation = Quaternion.identity;
            _child.SetZPosition(-1);
            Vector3IsEqual (_child.position, new Vector3 (2, 2, -1));
        }

        [Test]
        public void SetXLocalPosition ()
        {
            _child.SetXLocalPosition(-1);
            Vector3IsEqual (_child.localPosition, new Vector3 (-1, 1, 1));
        }

        [Test]
        public void SetYLocalPosition ()
        {
            _child.SetYLocalPosition(-1);
            Vector3IsEqual (_child.localPosition, new Vector3 (1, -1, 1));
        }

        [Test]
        public void SetZLocalPosition ()
        {
            _child.SetZLocalPosition(-1);
            Vector3IsEqual (_child.localPosition, new Vector3 (1, 1, -1));
        }

        [Test]
        public void SetXEulerAngle ()
        {
            _parent.rotation = Quaternion.identity;
            _child.SetXEulerAngle (10);
            Vector3IsEqual (_child.eulerAngles, new Vector3 (10, 5, 6));
        }

        [Test]
        public void SetYEulerAngle ()
        {
            _parent.rotation = Quaternion.identity;
            _child.SetYEulerAngle (10);
            Vector3IsEqual (_child.eulerAngles, new Vector3 (4, 10, 6));
        }

        [Test]
        public void SetZEulerAngle ()
        {
            _parent.rotation = Quaternion.identity;
            _child.SetZEulerAngle (10);
            Vector3IsEqual (_child.eulerAngles, new Vector3 (4, 5, 10));
        }

        [Test]
        public void SetXLocalEulerAngle ()
        {
            _child.SetXLocalEulerAngle (10);
            Vector3IsEqual (_child.localEulerAngles, new Vector3 (10, 5, 6));
        }

        [Test]
        public void SetYLocalEulerAngle ()
        {
            _child.SetYLocalEulerAngle (10);
            Vector3IsEqual (_child.localEulerAngles, new Vector3 (4, 10, 6));
        }

        [Test]
        public void SetZLocalEulerAngle ()
        {
            _child.SetZLocalEulerAngle (10);
            Vector3IsEqual (_child.localEulerAngles, new Vector3 (4, 5, 10));
        }

        private void Vector3IsEqual (Vector3 actual, Vector3 expected)
        {
            actual.x.ShouldBe (expected.x, 0.00001f,
                $"expected {expected.ToDetailedString ()}, but was {actual.ToDetailedString ()}");
            actual.y.ShouldBe (expected.y, 0.00001f,
                $"expected {expected.ToDetailedString ()}, but was {actual.ToDetailedString ()}");
            actual.z.ShouldBe (expected.z, 0.00001f,
                $"expected {expected.ToDetailedString ()}, but was {actual.ToDetailedString ()}");
        }

        private void QuaternionIsEqual (Quaternion actual, Quaternion expected)
        {
            Quaternion.Angle (actual, expected).ShouldBe (0, 0.00001f,
                $"eulerAngles expected {expected.eulerAngles.ToDetailedString ()}, but was {actual.eulerAngles.ToDetailedString ()}");
        }
    }
}
