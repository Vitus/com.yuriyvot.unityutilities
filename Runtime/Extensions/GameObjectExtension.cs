﻿using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions
{
    public static class GameObjectExtension
    {
        public static T GetOrAddComponent<T> (this GameObject go) where T : Component
            => go.GetComponent<T> () ?? go.AddComponent<T> ();
    }
}
