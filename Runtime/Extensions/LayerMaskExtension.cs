﻿using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions
{
    public static class LayerMaskExtension
    {
        public static bool Contains (this LayerMask mask, int layer)
            => mask == (mask | (1 << layer));
    }
}
