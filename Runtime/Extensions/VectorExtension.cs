﻿using System.Globalization;
using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions
{
    public static class VectorExtension
    {
        public static Vector3 ToXZVector3 (this Vector2 v)
            => new Vector3 (v.x, 0, v.y);

        public static Vector2 ToVector2ByXZ (this Vector3 v)
            => new Vector2 (v.x, v.z);

        public static Vector2 WithX (this Vector2 v, float x)
            => new Vector2 (x, v.y);

        public static Vector2 WithY (this Vector2 v, float y)
            => new Vector2 (v.x, y);

        public static Vector3 WithX (this Vector3 v, float x)
            => new Vector3 (x, v.y, v.z);

        public static Vector3 WithY (this Vector3 v, float y)
            => new Vector3 (v.x, y, v.z);

        public static Vector3 WithZ (this Vector3 v, float z)
            => new Vector3 (v.x, v.y, z);

        public static string ToStringWithDigits (this Vector2 v, int digitsAfterDot)
        {
            var format = $"0.{new string ('0', digitsAfterDot)}";
            return $"({v.x.ToString (format, CultureInfo.InvariantCulture)}, "
                   + $"{v.y.ToString (format, CultureInfo.InvariantCulture)})";
        }

        public static string ToStringWithDigits (this Vector3 v, int digitsAfterDot)
        {
            var format = $"0.{new string ('0', digitsAfterDot)}";
            return $"({v.x.ToString (format, CultureInfo.InvariantCulture)}, "
                   + $"{v.y.ToString (format, CultureInfo.InvariantCulture)}, "
                   + $"{v.z.ToString (format, CultureInfo.InvariantCulture)})";
        }

        public static string ToDetailedString (this Vector2 v)
        {
            return $"({v.x}, {v.y})";
        }

        public static string ToDetailedString (this Vector3 v)
        {
            return $"({v.x}, {v.y}, {v.z})";
        }

        public static Vector2 RotateClockwise (this Vector2 v, float angle)
            => Quaternion.Euler (0, 0, -angle) * v;
    }
}
