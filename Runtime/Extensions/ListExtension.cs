﻿using System;
using System.Collections.Generic;
using YuriyVot.UnityUtilities.Wrappers;

namespace YuriyVot.UnityUtilities.Extensions
{
    public static class ListExtension
    {
        public static T Random<T> (this IList<T> list, IRandom random)
        {
            if (list.Count == 0)
            {
                throw new IndexOutOfRangeException ("List needs at least one entry to call Random()");
            }
            return list[random.Range (0, list.Count)];
        }

        public static T RandomOrDefault<T> (this IList<T> list, IRandom random)
            => list.Count == 0 ? default : list.Random (random);

        public static T PopLast<T> (this IList<T> list)
        {
            if (list.Count == 0)
            {
                throw new InvalidOperationException ();
            }

            var t = list[list.Count - 1];
            list.RemoveAt (list.Count - 1);

            return t;
        }
    }
}
