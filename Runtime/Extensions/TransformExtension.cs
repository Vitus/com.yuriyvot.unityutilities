﻿using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions
{
    public static class TransformExtension
    {
        public static void ResetWorldScale (this Transform t)
        {
            t.localScale = Vector3.one;
            var lossyScale = t.lossyScale;
            t.localScale = new Vector3 (
                (float) (1 / (double) lossyScale.x),
                (float) (1 / (double) lossyScale.y),
                (float) (1 / (double) lossyScale.z)
            );
        }

        public static void SetUniformWorldScale (this Transform t, float scale)
        {
            t.localScale = Vector3.one;
            var lossyScale = t.lossyScale;
            t.localScale = new Vector3 (
                (float) (scale / (double) lossyScale.x),
                (float) (scale / (double) lossyScale.y),
                (float) (scale / (double) lossyScale.z)
            );
        }

        public static void SetWorldScale (this Transform t, float scaleX, float scaleY, float scaleZ)
        {
            t.localScale = Vector3.one;
            var lossyScale = t.lossyScale;
            t.localScale = new Vector3 (
                (float) (scaleX / (double) lossyScale.x),
                (float) (scaleY / (double) lossyScale.y),
                (float) (scaleZ / (double) lossyScale.z)
            );
        }

        public static void ResetLocation (this Transform t)
        {
            t.SetPositionAndRotation (Vector3.zero, Quaternion.identity);
        }

        public static void ResetLocalLocation (this Transform t)
        {
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
        }

        public static void ResetLocalLocationAndScale (this Transform t)
        {
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
        }

        public static void CopyLocation (this Transform t, Transform target)
        {
            t.SetPositionAndRotation (target.position, target.rotation);
        }

        public static void CopyLocalLocation (this Transform t, Transform target)
        {
            t.localPosition = target.localPosition;
            t.localRotation = target.localRotation;
        }

        public static void CopyLocalLocationAndScale (this Transform t, Transform target)
        {
            t.localPosition = target.localPosition;
            t.localRotation = target.localRotation;
            t.localScale = target.localScale;
        }

        public static void SetXLocalScale (this Transform t, float x)
        {
            var scale = t.localScale;
            scale.x = x;
            t.localScale = scale;
        }

        public static void SetYLocalScale (this Transform t, float y)
        {
            var scale = t.localScale;
            scale.y = y;
            t.localScale = scale;
        }

        public static void SetZLocalScale (this Transform t, float z)
        {
            var scale = t.localScale;
            scale.z = z;
            t.localScale = scale;
        }

        public static void SetUniformLocalScale (this Transform t, float scale)
        {
            t.localScale = Vector3.one * scale;
        }

        public static void SetXPosition (this Transform t, float x)
        {
            var position = t.position;
            position.x = x;
            t.position = position;
        }

        public static void SetYPosition (this Transform t, float y)
        {
            var position = t.position;
            position.y = y;
            t.position = position;
        }

        public static void SetZPosition (this Transform t, float z)
        {
            var position = t.position;
            position.z = z;
            t.position = position;
        }

        public static void SetXLocalPosition (this Transform t, float x)
        {
            var localPosition = t.localPosition;
            localPosition.x = x;
            t.localPosition = localPosition;
        }

        public static void SetYLocalPosition (this Transform t, float y)
        {
            var localPosition = t.localPosition;
            localPosition.y = y;
            t.localPosition = localPosition;
        }

        public static void SetZLocalPosition (this Transform t, float z)
        {
            var localPosition = t.localPosition;
            localPosition.z = z;
            t.localPosition = localPosition;
        }

        public static void SetXEulerAngle (this Transform t, float x)
        {
            var eulerAngles = t.eulerAngles;
            eulerAngles.x = x;
            t.eulerAngles = eulerAngles;
        }

        public static void SetYEulerAngle (this Transform t, float y)
        {
            var eulerAngles = t.eulerAngles;
            eulerAngles.y = y;
            t.eulerAngles = eulerAngles;
        }

        public static void SetZEulerAngle (this Transform t, float z)
        {
            var eulerAngles = t.eulerAngles;
            eulerAngles.z = z;
            t.eulerAngles = eulerAngles;
        }

        public static void SetXLocalEulerAngle (this Transform t, float x)
        {
            var localEulerAngles = t.localEulerAngles;
            localEulerAngles.x = x;
            t.localEulerAngles = localEulerAngles;
        }

        public static void SetYLocalEulerAngle (this Transform t, float y)
        {
            var localEulerAngles = t.localEulerAngles;
            localEulerAngles.y = y;
            t.localEulerAngles = localEulerAngles;
        }

        public static void SetZLocalEulerAngle (this Transform t, float z)
        {
            var localEulerAngles = t.localEulerAngles;
            localEulerAngles.z = z;
            t.localEulerAngles = localEulerAngles;
        }
    }
}
