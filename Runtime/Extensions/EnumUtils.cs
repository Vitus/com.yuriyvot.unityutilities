using System;
using YuriyVot.UnityUtilities.Wrappers;

namespace YuriyVot.UnityUtilities.Extensions
{
    public static class EnumUtils
    {
        public static T Random<T> (IRandom random)
        {
            var t = typeof(T);
            if (!t.IsEnum)
            {
                return default;
            }

            var values = Enum.GetValues (t);

            return (T) values.GetValue (random.Range (0, values.Length));
        }
    }
}
