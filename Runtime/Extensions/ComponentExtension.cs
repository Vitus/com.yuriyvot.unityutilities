﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace YuriyVot.UnityUtilities.Extensions
{
    public static class ComponentExtension
    {
        public static T GetOrAddComponent<T> (this Component mb) where T : Component
            => mb.gameObject.GetOrAddComponent<T> ();

        public static T GetComponentNonAlloc<T> (this Component mb, List<T> buffer)
        {
            mb.GetComponents (buffer);
            return buffer.FirstOrDefault ();
        }
    }
}
