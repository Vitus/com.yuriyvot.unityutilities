using UnityEngine;
using YuriyVot.UnityUtilities.Wrappers;

namespace YuriyVot.UnityUtilities.ExtendedPlayerPrefs
{
    public class ExtendedPrefsStorage : IExtendedPrefsStorage
    {
        private readonly IPrefsStorage _storage;

        public ExtendedPrefsStorage (IPrefsStorage storage) => _storage = storage;

        public void DeleteKey (string key)
        {
            _storage.DeleteKey (key);
            _storage.DeleteKey ($"{key}[.x]");
            _storage.DeleteKey ($"{key}[.y]");
            _storage.DeleteKey ($"{key}[.z]");
        }

        public bool HasKey (string key)
            => _storage.HasKey (key) || _storage.HasKey ($"{key}[.x]");

        public bool GetBool (string key, bool defaultValue = false)
            => _storage.GetInt (key, defaultValue ? 1 : 0) == 1;

        public void SetBool (string key, bool value)
            => _storage.SetInt (key, value ? 1 : 0);

        public Vector3 GetVector3 (string key, Vector3 defaultValue = default)
        {
            var x = _storage.GetFloat ($"{key}[.x]", defaultValue.x);
            var y = _storage.GetFloat ($"{key}[.y]", defaultValue.y);
            var z = _storage.GetFloat ($"{key}[.z]", defaultValue.z);

            return new Vector3 (x, y, z);
        }

        public void SetVector3 (string key, Vector3 value)
        {
            _storage.SetFloat ($"{key}[.x]", value.x);
            _storage.SetFloat ($"{key}[.y]", value.y);
            _storage.SetFloat ($"{key}[.z]", value.z);
        }

        public void DeleteAll ()
            => _storage.DeleteAll ();

        public int GetInt (string key, int defaultValue = default)
            => _storage.GetInt (key, defaultValue);

        public void SetInt (string key, int value)
            => _storage.SetInt (key, value);

        public float GetFloat (string key, float defaultValue = default)
            => _storage.GetFloat (key, defaultValue);

        public void SetFloat (string key, float value)
            => _storage.SetFloat (key, value);

        public string GetString (string key, string defaultValue = default)
            => _storage.GetString (key, defaultValue);

        public void SetString (string key, string value)
            => _storage.SetString (key, value);
    }
}
