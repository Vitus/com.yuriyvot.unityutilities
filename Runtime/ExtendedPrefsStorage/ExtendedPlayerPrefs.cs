using YuriyVot.UnityUtilities.Wrappers;

namespace YuriyVot.UnityUtilities.ExtendedPlayerPrefs
{
    public class ExtendedPlayerPrefs : ExtendedPrefsStorage
    {
        public static readonly ExtendedPlayerPrefs Instance = new ExtendedPlayerPrefs ();
        public ExtendedPlayerPrefs () : base (new UnityPlayerPrefs ()) { }
    }
}
