using UnityEngine;
using YuriyVot.UnityUtilities.Wrappers;

namespace YuriyVot.UnityUtilities.ExtendedPlayerPrefs
{
    public interface IExtendedPrefsStorage : IPrefsStorage
    {
        bool GetBool (string key, bool defaultValue = false);
        void SetBool (string key, bool value);
        Vector3 GetVector3 (string key, Vector3 defaultValue = default);
        void SetVector3 (string key, Vector3 value);
    }
}
