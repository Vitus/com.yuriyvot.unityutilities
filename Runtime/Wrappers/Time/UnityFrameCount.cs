﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnityFrameCount : IFrameCounter
    {
        public static UnityFrameCount Instance { get; } = new UnityFrameCount ();

        public int FrameCount
            => UnityEngine.Time.frameCount;
    }
}
