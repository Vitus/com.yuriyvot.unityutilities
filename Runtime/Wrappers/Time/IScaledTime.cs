﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public interface IScaledTime : ITime
    {
        float TimeScale { get; set; }
    }
}
