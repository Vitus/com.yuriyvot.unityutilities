﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnitySmoothTime : IScaledTime
    {
        public static UnitySmoothTime Instance { get; } = new UnitySmoothTime ();

        public float Time
            => UnityEngine.Time.time;

        public double TimeAsDouble
            => UnityEngine.Time.timeAsDouble;

        public float DeltaTime
            => UnityEngine.Time.smoothDeltaTime;

        public float MaxDeltaTime
        {
            get => UnityEngine.Time.maximumDeltaTime;
            set => UnityEngine.Time.maximumDeltaTime = value;
        }

        public float TimeScale
        {
            get => UnityEngine.Time.timeScale;
            set => UnityEngine.Time.timeScale = value;
        }
    }
}
