﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnityFixedTime : IScaledTime
    {
        public static UnityFixedTime Instance { get; } = new UnityFixedTime ();

        public float Time
            => UnityEngine.Time.fixedTime;

        public double TimeAsDouble
            => UnityEngine.Time.fixedTimeAsDouble;

        public float DeltaTime
            => UnityEngine.Time.fixedDeltaTime;

        public float MaxDeltaTime
        {
            get => UnityEngine.Time.maximumDeltaTime;
            set => UnityEngine.Time.maximumDeltaTime = value;
        }

        public float TimeScale
        {
            get => UnityEngine.Time.timeScale;
            set => UnityEngine.Time.timeScale = value;
        }
    }
}
