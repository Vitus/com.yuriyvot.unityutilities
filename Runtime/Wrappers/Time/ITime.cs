﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public interface ITime
    {
        float Time { get; }
        double TimeAsDouble { get; }
        float DeltaTime { get; }
        float MaxDeltaTime { get; set; }
    }
}
