﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnityFixedUnscaledTime : ITime
    {
        public static UnityFixedUnscaledTime Instance { get; } = new UnityFixedUnscaledTime ();

        public float Time
            => UnityEngine.Time.fixedUnscaledTime;

        public double TimeAsDouble
            => UnityEngine.Time.fixedUnscaledTimeAsDouble;

        public float DeltaTime
            => UnityEngine.Time.fixedUnscaledDeltaTime;

        public float MaxDeltaTime
        {
            get => UnityEngine.Time.maximumDeltaTime;
            set => UnityEngine.Time.maximumDeltaTime = value;
        }
    }
}
