﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnityRealTime : IScaledTime
    {
        public static UnityRealTime Instance { get; } = new UnityRealTime ();

        public float Time
            => UnityEngine.Time.realtimeSinceStartup;

        public double TimeAsDouble
            => UnityEngine.Time.realtimeSinceStartupAsDouble;

        public float DeltaTime
            => UnityEngine.Time.unscaledDeltaTime;

        public float MaxDeltaTime
        {
            get => UnityEngine.Time.maximumDeltaTime;
            set => UnityEngine.Time.maximumDeltaTime = value;
        }

        public float TimeScale
        {
            get => UnityEngine.Time.timeScale;
            set => UnityEngine.Time.timeScale = value;
        }
    }
}
