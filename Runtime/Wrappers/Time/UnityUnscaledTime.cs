﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnityUnscaledTime : ITime
    {
        public static UnityUnscaledTime Instance { get; } = new UnityUnscaledTime ();

        public float Time
            => UnityEngine.Time.unscaledTime;

        public double TimeAsDouble
            => UnityEngine.Time.unscaledTimeAsDouble;

        public float DeltaTime
            => UnityEngine.Time.unscaledDeltaTime;

        public float MaxDeltaTime
        {
            get => UnityEngine.Time.maximumDeltaTime;
            set => UnityEngine.Time.maximumDeltaTime = value;
        }
    }
}
