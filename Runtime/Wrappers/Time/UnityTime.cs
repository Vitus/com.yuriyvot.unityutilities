﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnityTime : IScaledTime
    {
        public static UnityTime Instance { get; } = new UnityTime ();

        public float Time
            => UnityEngine.Time.time;

        public double TimeAsDouble
            => UnityEngine.Time.timeAsDouble;

        public float DeltaTime
            => UnityEngine.Time.deltaTime;

        public float MaxDeltaTime
        {
            get => UnityEngine.Time.maximumDeltaTime;
            set => UnityEngine.Time.maximumDeltaTime = value;
        }

        public float TimeScale
        {
            get => UnityEngine.Time.timeScale;
            set => UnityEngine.Time.timeScale = value;
        }
    }
}
