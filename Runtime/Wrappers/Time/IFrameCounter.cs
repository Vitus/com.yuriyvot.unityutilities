﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public interface IFrameCounter
    {
        int FrameCount { get; }
    }
}