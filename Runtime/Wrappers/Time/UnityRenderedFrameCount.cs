﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnityRenderedFrameCount : IFrameCounter
    {
        public static UnityRenderedFrameCount Instance { get; } = new UnityRenderedFrameCount ();

        public int FrameCount
            => UnityEngine.Time.renderedFrameCount;
    }
}
