﻿namespace YuriyVot.UnityUtilities.Wrappers
{
    public class UnityLevelTime : IScaledTime
    {
        public static UnityLevelTime Instance { get; } = new UnityLevelTime ();

        public float Time
            => UnityEngine.Time.timeSinceLevelLoad;

        public double TimeAsDouble
            => UnityEngine.Time.timeSinceLevelLoadAsDouble;

        public float DeltaTime
            => UnityEngine.Time.deltaTime;

        public float MaxDeltaTime
        {
            get => UnityEngine.Time.maximumDeltaTime;
            set => UnityEngine.Time.maximumDeltaTime = value;
        }

        public float TimeScale
        {
            get => UnityEngine.Time.timeScale;
            set => UnityEngine.Time.timeScale = value;
        }
    }
}
